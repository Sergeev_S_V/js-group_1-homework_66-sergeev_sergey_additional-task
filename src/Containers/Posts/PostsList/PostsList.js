import React, {Component} from 'react';
import axios from 'axios';
import Post from "../../../Components/Posts/Post/Post";
import ErrorBoundary from "../../../Components/UI/ErrorBoundary/ErrorBoundary";

class PostsList extends Component {

  state = {
    posts: {},
  };

  getPostsFromAPI = () => {
    this.setState(() => {
      axios.get('/posts.json')
        .then(response => {
          if (response) {
            this.setState({posts: response.data});
          }
        });
    })
  };

  readMoreHandler = (key) => {
    this.props.history.push(`/posts/${key}`);
  };

  componentDidMount() {
    this.getPostsFromAPI();
  };

  render() {
    return(
      <div className="container">
        {this.state.posts
          ? Object.keys(this.state.posts).map(key => (
            <ErrorBoundary key={key}>
              <Post title={this.state.posts[key].title}
                    body={this.state.posts[key].body}
                    date={this.state.posts[key].date}
                    clicked={() => this.readMoreHandler(key)}/>
            </ErrorBoundary>
          ))
          : <p style={{fontWeight: 'bold'}}>Add new post</p>}
      </div>
    );
  }
}

export default PostsList;