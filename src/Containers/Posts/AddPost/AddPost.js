import React, {Component} from 'react';
import axios from 'axios';
import Spinner from "../../../Components/UI/Spinner/Spinner";
import AddPostForm from "../../../Components/Posts/AddPostForm/AddPostForm";


class AddPost extends Component {

  state = {
    title: '',
    body: '',
    date: '26.02.18',
    loading: false,
  };

  titleChangeHandler = event => {
    this.setState({title: event.target.value});
  };

  bodyChangeHandler = event => {
    this.setState({body: event.target.value});
  };

  addPostHandler = () => {
    this.props.match.params.id
      ? this.setState(() => {
        axios.patch(`/posts/${this.props.match.params.id}.json`,
          {title: this.state.title, body: this.state.body, date: this.state.date})
          .then(response => {
            if (response) {
              this.props.history.push('/');
            }
          })
      })
      : this.state.title.length &&
      this.state.body.length &&
      this.state.date.length > 0 ?
        this.setState(() => {
          axios.post(`/posts/.json`,
            {title: this.state.title, body: this.state.body, date: this.state.date})
            .then(response => {
              if (response) {
                this.props.history.push('/');
              }
            });
        }) : null;
  };

  componentDidUpdate(prevProps) {
    prevProps.match.params.id !== this.props.match.params.id
      ? this.setState({title: '', body: ''})
      : null
  }

  componentDidMount() {
    this.setState({loading: true}, () => {
      this.props.match.params.id
        ? axios.get(`/posts/${this.props.match.params.id}.json`)
          .then(response => {
            this.setState({
              title: response.data.title,
              body: response.data.body,
              date: response.data.date,
              loading: false})
          })
        : this.setState({loading: false});
    });
  }

  render() {
    let addPost = (
      <div className="AddPost">
        <AddPostForm changeTitle={(event) => this.titleChangeHandler(event)}
                     changeBody={(event) => this.bodyChangeHandler(event)}
                     date={this.state.date}
                     title={this.state.title}
                     body={this.state.body}
                     add={() => this.addPostHandler()}
        />
      </div>
    );

    this.state.loading ? addPost = <Spinner/> : null;

    return(
      <div className='container'>
        {addPost}
      </div>
    );
  }
}

export default AddPost;