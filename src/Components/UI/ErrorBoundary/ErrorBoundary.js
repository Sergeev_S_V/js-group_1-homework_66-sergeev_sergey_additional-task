import React, {Component} from 'react';
import './ErrorBoundary.css';
import axios from 'axios';
import Spinner from "../Spinner/Spinner";

class ErrorBoundary extends Component {
  state = {
    hasError: false,
    errorMessage: '',
    loading: true,
  };

  componentDidCatch(error, info) {
    this.setState({hasError: true, errorMessage: error}, () => {
      axios.post(`/errors.json`, {errorMessage: `${error}`})
        .then(() => this.setState({loading: false}));
    });
  };

  render() {
    if (this.state.hasError && this.state.loading) {
      return <Spinner loading={this.state.loading}/>
    }
    if (this.state.hasError) {
      return <div className='Error'>Something wrong happened</div>
    } else {
      return this.props.children;
    }
  }
}

export default ErrorBoundary;