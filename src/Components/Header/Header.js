import React from 'react';
import { NavLink } from 'react-router-dom';

import './Header.css';

const Header = () => (
  <header>
    <div className='container'>
      <div className="row">
        <div className="col-md-7">
          <NavLink exact to='/'>Blog</NavLink>
        </div>
        <div className="col-md-5">
          <ul>
            <li>
              <NavLink exact to='/'>Home</NavLink>
            </li>
            <li>
              <NavLink exact to='/posts/add'>Add</NavLink>
            </li>
            <li>
              <NavLink exact to='/about'>About</NavLink>
            </li>
            <li>
              <NavLink exact to='/contacts'>Contacts</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </header>
);

export default Header;