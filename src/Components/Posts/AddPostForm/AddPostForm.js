import React, {Fragment} from 'react';
import Button from "../../UI/Button/Button";

const AddPostForm = props => {

  return (
    <Fragment>
      <h1>Add new post</h1>
      <form>
        <div className="form-group">
          <p style={{fontWeight: 'bold'}}>Created on: {props.date}</p>
          <label>Title</label>
          <input type="text" onChange={props.changeTitle} name='title' value={props.title} className="form-control"/>
        </div>
        <div className="form-group">
          <label>Description</label>
          <textarea onChange={props.changeBody} value={props.body} className="form-control" rows="3"/>
        </div>
      </form>
      <Button clicked={props.add}>Save</Button>
    </Fragment>
  );
};

export default AddPostForm;