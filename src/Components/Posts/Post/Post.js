import React from 'react';
import Button from "../../UI/Button/Button";
import './Post.css';

const Post = props => {
  if (Math.random() > 0.8) {
    throw new Error('Was error');
  }
  return <div className='Post'>
    <p>Created on: {props.date}</p>
    <p>{props.title}</p>
    <p>{props.body}</p>
    <Button clicked={props.clicked}>Read more >></Button>
  </div>
};

export default Post;